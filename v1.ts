interface Entity {
    readonly x: number;
    readonly y: number;
}

interface NPCEntity extends Entity {
    readonly name: string;
}

interface Wrench extends Entity {}

interface Bot extends Entity {
    readonly id: number;
    readonly wrenches: number;

    moveTo(thing: Entity);
    build();
    collect();
    attack(enemy: Bot);
}

interface World {
    readonly wrenches: Wrench[];
    readonly bots: Bot[];
    readonly others: Bot[];
    readonly turn: number;
}

namespace Util {
    export function pad(str: string, width: number, char: string = ' ') {
        str = str + '';

        return str.length >= width ? str : new Array(width - str.length + 1).join(char) + str;
    }
}

namespace Logger {
    export enum Verbosity {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        CRITICAL,
        MUTED,
    }

    var verbosity = Verbosity.ERROR;

    export function setLevel(level: Verbosity) {
        verbosity = level;
    }

    export function log(level: Verbosity, message: string, speaker: Entity = null, context: Object = null) {
        if (level < verbosity)
            return;

        let msg = `[${Verbosity[level]}] `;

        if (speaker)
            msg += getSpeakerText(speaker);

        msg += message;

        if (context)
            msg += ` ${JSON.stringify(context)}`;

        console.log(msg.trim());
    }

    export function debug(message: string, speaker: Entity = null, context: Object = null) {
        log(Verbosity.DEBUG, message, speaker, context);
    }

    export function info(message: string, speaker: Entity = null, context: Object = null) {
        log(Verbosity.INFO, message, speaker, context);
    }

    export function warning(message: string, speaker: Entity = null, context: Object = null) {
        log(Verbosity.WARNING, message, speaker, context);
    }

    export function error(message: string, speaker: Entity = null, context: Object = null) {
        log(Verbosity.ERROR, message, speaker, context);
    }

    export function critical(message: string, speaker: Entity = null, context: Object = null) {
        log(Verbosity.CRITICAL, message, speaker, context);
    }

    function getSpeakerText(entity: Entity) {
        let type = Entity.getType(entity);
        let n = Entity.Type[type];
        
        let text = `${n.charAt(0)}${n.substr(1).toLowerCase()}`;

        if (type === Entity.Type.NPC)
            text += `.${(<NPCEntity>entity).name}`;
        else if (type === Entity.Type.BOT)
            text += `#${(<Bot>entity).id}`;
        else if (type === Entity.Type.WRENCH)
            text += `@(${entity.x}, ${entity.y})`;
        else
            text += '(?)';

        return text + ' says: ';
    }
}

namespace Entity {
    export enum Type {
        UNKNOWN,
        BOT,
        WRENCH,
        CASTLE,
        NPC,
    }

    export function getType(thing): Type {
        if (!thing)
            return Type.UNKNOWN;

        if (thing.name !== void 0)
            return Type.NPC;
        else if (thing.wrenches !== void 0)
            return Type.BOT;
        
        let propNames = Object.keys(thing);

        if (propNames.length === 2 && propNames.indexOf('x') >= 0 && propNames.indexOf('y') >= 0)
            return Type.WRENCH;

        return Type.UNKNOWN;
    }

    export function distance(a: Entity, b: Entity): number {
        return Math.max(Math.abs(a.x - b.x), Math.abs(a.y - b.y));
    }

    export function nearest(subject: Entity, others: Entity[], exclude: Entity[] = []): Entity {
        if (exclude)
            others = others.filter(item => exclude.indexOf(item) === -1);

        if (!others.length)
            return null;

        let nearest = others[0];
        let dist = distance(subject, nearest);

        for (let i = 1, ii = others.length; i < ii; i++) {
            let d = distance(subject, others[i]);

            if (d > dist)
                continue;

            nearest = others[i];
            dist = d;
        }

        return nearest;
    }

    export function findInWorld(subject: Entity, world: World) {
        let type = getType(subject);

        if (type === Type.BOT)
            findBotInWorld(<Bot>subject, world);
        else if (type === Type.WRENCH)
            findWrenchInWorld(<Wrench>subject, world);

        return null;
    }

    function findBotInWorld(subject: Bot, world: World) {
        for (let i = 0, ii = world.bots.length; i < ii; i++)
            if (world.bots[i].id === subject.id)
                return world.bots[i];

        return null;
    }

    function findWrenchInWorld(subject: Wrench, world: World) {
        for (let i = 0, ii = world.wrenches.length; i < ii; i++) {
            let wrench = world.wrenches[i];

            if (wrench.x === subject.x && wrench.y === subject.y)
                return wrench;
        }

        return null;
    }
}

namespace BotState {
    const ATTACK_LINES = [
        'Whoooowee! Makin\' bacon!',
        'How\'d that plan turn out for ya, dummy?',
        'That\'s what ya get!',
        'Take it like a man, shorty.',
        'Erectin\' a statue of a moron.',
        'You done incurred my wrath, son!',
        'You just ain\'t doin\' it right.',
        'Dominated, hardhat.',
    ];

    var states = [];
    var stateMap = {};

    var hunted = new Set<Entity>();

    export function getHunted(): Entity[] {
        return Array.from(hunted);
    }

    export enum Actions {
        IDLE,
        SEEKING,
        BUILDING,
        COLLECTING,
        MOVING,
        ATTACKING,
    };

    export class State implements Entity {
        public base: Bot;

        private _action: Actions = Actions.IDLE;
        private _seeking: Entity = null;

        constructor(base: Bot) {
            this.base = base;
        }

        get id(): number {
            return this.base.id;
        }

        get x(): number {
            return this.base.x;
        }

        get y(): number {
            return this.base.y;
        }

        get wrenches(): number {
            return this.base.wrenches;
        }

        get action(): Actions {
            return this._action;
        }

        get seeking(): Entity {
            return this._seeking;
        }

        is(action: Actions): boolean {
            return this.action === action;
        }

        moveTo(target: Entity) {
            Logger.info('Yeah, yeah... I\'m going.', this, { target: target });

            this._action = Actions.MOVING;

            this.base.moveTo(target);

            this._action = Actions.IDLE;
        }

        collect() {
            Logger.info('Oooh... Shiny!', this);

            this._action = Actions.COLLECTING;

            this.base.collect();

            this._action = Actions.IDLE;
        }

        build() {
            Logger.info('Go on, get!', this);

            this._action = Actions.BUILDING;

            this.base.build();

            this._action = Actions.IDLE;
        }

        attack(target: Bot) {
            Logger.info(ATTACK_LINES[Math.floor(ATTACK_LINES.length * Math.random())], this, { target: target });

            let lastAction = this._action;
            this._action = Actions.ATTACKING;

            this.base.attack(target);

            this._action = lastAction;
        }

        seek(thing: Entity) {
            if (!thing) {
                if (this._seeking)
                    hunted.delete(this._seeking);

                Logger.info('Target lock cleared', this);

                this._action = Actions.IDLE;
                this._seeking = null;

                return;
            }

            if (hunted.has(thing)) {
                Logger.warning('Someone else is already hunting that...', this, { thing: thing });

                return;
            }

            Logger.info('Target aquired', this, { target: thing });

            this._action = Actions.SEEKING;
            this._seeking = thing;

            hunted.add(thing);
        }

        refresh(bot: Bot, world: World) {
            this.base = bot;

            if (this.action === Actions.SEEKING) {
                let target = Entity.findInWorld(this.seeking, world);

                Logger.debug(target === this._seeking ? 'I refreshed, but my target is still the same...' : 'Refreshed properly :)', this);

                if (target) {
                    hunted.delete(this._seeking);
                    hunted.add(target);

                    this._seeking = target;
                } else
                    this.seek(null);
            }
        }
    }

    export function add(bot: Bot) {
        if (stateMap[bot.id] !== void 0)
            return;

        states.push(new State(bot));
        stateMap[bot.id] = states.length - 1;
    }

    export function addAll(bots: Bot[]) {
        bots.forEach(bot => add(bot));
    }

    export function get(bot: Bot): State {
        if (stateMap[bot.id] === void 0)
            return null;

        return states[stateMap[bot.id]];
    }

    export function getAll(): State[] {
        return states;
    }

    export function refresh(bot: Bot, world: World) {
        Logger.info('Refreshing bot', null, { bot: bot });

        let state = get(bot);

        if (!state) {
            if (world.turn > 0)
                Logger.warning('Bot not found in tracked states!');

            add(bot);

            return;
        }

        state.refresh(bot, world);
    }

    export function refreshAll(world: World) {
        Logger.info('Refreshing all bot states...');

        world.bots.forEach(bot => refresh(bot, world));
    }
}

const BUILD_THRESHOLD = 3;

Logger.setLevel(Logger.Verbosity.DEBUG);

function play(world: World) {
    BotState.refreshAll(world);

    BotState.getAll().forEach(bot => {
        let adjacentEnemy = Entity.nearest(bot, world.others);

        if (adjacentEnemy && Entity.distance(bot, adjacentEnemy) === 1) {
            bot.attack(<Bot>adjacentEnemy);

            return;
        }

        if (bot.is(BotState.Actions.IDLE) && bot.wrenches < BUILD_THRESHOLD)
            bot.seek(Entity.nearest(bot, world.wrenches, BotState.getHunted()));

        if (bot.is(BotState.Actions.SEEKING)) {
            let target = bot.seeking;

            if (bot.x === target.x && bot.y === target.y)
                bot.collect();
            else
                bot.moveTo(target);
        } else if (bot.wrenches >= BUILD_THRESHOLD)
            bot.build();
    });
}
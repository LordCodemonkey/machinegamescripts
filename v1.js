var Util;
(function (Util) {
    function pad(str, width, char = ' ') {
        str = str + '';
        return str.length >= width ? str : new Array(width - str.length + 1).join(char) + str;
    }
    Util.pad = pad;
})(Util || (Util = {}));
var Logger;
(function (Logger) {
    (function (Verbosity) {
        Verbosity[Verbosity["DEBUG"] = 0] = "DEBUG";
        Verbosity[Verbosity["INFO"] = 1] = "INFO";
        Verbosity[Verbosity["WARNING"] = 2] = "WARNING";
        Verbosity[Verbosity["ERROR"] = 3] = "ERROR";
        Verbosity[Verbosity["CRITICAL"] = 4] = "CRITICAL";
        Verbosity[Verbosity["MUTED"] = 5] = "MUTED";
    })(Logger.Verbosity || (Logger.Verbosity = {}));
    var Verbosity = Logger.Verbosity;
    var verbosity = Verbosity.ERROR;
    function setLevel(level) {
        verbosity = level;
    }
    Logger.setLevel = setLevel;
    function log(level, message, speaker = null, context = null) {
        if (level < verbosity)
            return;
        let msg = `[${Verbosity[level]}] `;
        if (speaker)
            msg += getSpeakerText(speaker);
        msg += message;
        if (context)
            msg += ` ${JSON.stringify(context)}`;
        console.log(msg.trim());
    }
    Logger.log = log;
    function debug(message, speaker = null, context = null) {
        log(Verbosity.DEBUG, message, speaker, context);
    }
    Logger.debug = debug;
    function info(message, speaker = null, context = null) {
        log(Verbosity.INFO, message, speaker, context);
    }
    Logger.info = info;
    function warning(message, speaker = null, context = null) {
        log(Verbosity.WARNING, message, speaker, context);
    }
    Logger.warning = warning;
    function error(message, speaker = null, context = null) {
        log(Verbosity.ERROR, message, speaker, context);
    }
    Logger.error = error;
    function critical(message, speaker = null, context = null) {
        log(Verbosity.CRITICAL, message, speaker, context);
    }
    Logger.critical = critical;
    function getSpeakerText(entity) {
        let type = Entity.getType(entity);
        let n = Entity.Type[type];
        let text = `${n.charAt(0)}${n.substr(1).toLowerCase()}`;
        if (type === Entity.Type.NPC)
            text += `.${entity.name}`;
        else if (type === Entity.Type.BOT)
            text += `#${entity.id}`;
        else if (type === Entity.Type.WRENCH)
            text += `@(${entity.x}, ${entity.y})`;
        else
            text += '(?)';
        return text + ' says: ';
    }
})(Logger || (Logger = {}));
var Entity;
(function (Entity) {
    (function (Type) {
        Type[Type["UNKNOWN"] = 0] = "UNKNOWN";
        Type[Type["BOT"] = 1] = "BOT";
        Type[Type["WRENCH"] = 2] = "WRENCH";
        Type[Type["CASTLE"] = 3] = "CASTLE";
        Type[Type["NPC"] = 4] = "NPC";
    })(Entity.Type || (Entity.Type = {}));
    var Type = Entity.Type;
    function getType(thing) {
        if (!thing)
            return Type.UNKNOWN;
        if (thing.name !== void 0)
            return Type.NPC;
        else if (thing.wrenches !== void 0)
            return Type.BOT;
        let propNames = Object.keys(thing);
        if (propNames.length === 2 && propNames.indexOf('x') >= 0 && propNames.indexOf('y') >= 0)
            return Type.WRENCH;
        return Type.UNKNOWN;
    }
    Entity.getType = getType;
    function distance(a, b) {
        return Math.max(Math.abs(a.x - b.x), Math.abs(a.y - b.y));
    }
    Entity.distance = distance;
    function nearest(subject, others, exclude = []) {
        if (exclude)
            others = others.filter(item => exclude.indexOf(item) === -1);
        if (!others.length)
            return null;
        let nearest = others[0];
        let dist = distance(subject, nearest);
        for (let i = 1, ii = others.length; i < ii; i++) {
            let d = distance(subject, others[i]);
            if (d > dist)
                continue;
            nearest = others[i];
            dist = d;
        }
        return nearest;
    }
    Entity.nearest = nearest;
    function findInWorld(subject, world) {
        let type = getType(subject);
        if (type === Type.BOT)
            findBotInWorld(subject, world);
        else if (type === Type.WRENCH)
            findWrenchInWorld(subject, world);
        return null;
    }
    Entity.findInWorld = findInWorld;
    function findBotInWorld(subject, world) {
        for (let i = 0, ii = world.bots.length; i < ii; i++)
            if (world.bots[i].id === subject.id)
                return world.bots[i];
        return null;
    }
    function findWrenchInWorld(subject, world) {
        for (let i = 0, ii = world.wrenches.length; i < ii; i++) {
            let wrench = world.wrenches[i];
            if (wrench.x === subject.x && wrench.y === subject.y)
                return wrench;
        }
        return null;
    }
})(Entity || (Entity = {}));
var BotState;
(function (BotState) {
    const ATTACK_LINES = [
        'Whoooowee! Makin\' bacon!',
        'How\'d that plan turn out for ya, dummy?',
        'That\'s what ya get!',
        'Take it like a man, shorty.',
        'Erectin\' a statue of a moron.',
        'You done incurred my wrath, son!',
        'You just ain\'t doin\' it right.',
        'Dominated, hardhat.',
    ];
    var states = [];
    var stateMap = {};
    var hunted = new Set();
    function getHunted() {
        return Array.from(hunted);
    }
    BotState.getHunted = getHunted;
    (function (Actions) {
        Actions[Actions["IDLE"] = 0] = "IDLE";
        Actions[Actions["SEEKING"] = 1] = "SEEKING";
        Actions[Actions["BUILDING"] = 2] = "BUILDING";
        Actions[Actions["COLLECTING"] = 3] = "COLLECTING";
        Actions[Actions["MOVING"] = 4] = "MOVING";
        Actions[Actions["ATTACKING"] = 5] = "ATTACKING";
    })(BotState.Actions || (BotState.Actions = {}));
    var Actions = BotState.Actions;
    ;
    class State {
        constructor(base) {
            this._action = Actions.IDLE;
            this._seeking = null;
            this.base = base;
        }
        get id() {
            return this.base.id;
        }
        get x() {
            return this.base.x;
        }
        get y() {
            return this.base.y;
        }
        get wrenches() {
            return this.base.wrenches;
        }
        get action() {
            return this._action;
        }
        get seeking() {
            return this._seeking;
        }
        is(action) {
            return this.action === action;
        }
        moveTo(target) {
            Logger.info('Yeah, yeah... I\'m going.', this, { target: target });
            this._action = Actions.MOVING;
            this.base.moveTo(target);
            this._action = Actions.IDLE;
        }
        collect() {
            Logger.info('Oooh... Shiny!', this);
            this._action = Actions.COLLECTING;
            this.base.collect();
            this._action = Actions.IDLE;
        }
        build() {
            Logger.info('Go on, get!', this);
            this._action = Actions.BUILDING;
            this.base.build();
            this._action = Actions.IDLE;
        }
        attack(target) {
            Logger.info(ATTACK_LINES[Math.floor(ATTACK_LINES.length * Math.random())], this, { target: target });
            let lastAction = this._action;
            this._action = Actions.ATTACKING;
            this.base.attack(target);
            this._action = lastAction;
        }
        seek(thing) {
            if (!thing) {
                if (this._seeking)
                    hunted.delete(this._seeking);
                Logger.info('Target lock cleared', this);
                this._action = Actions.IDLE;
                this._seeking = null;
                return;
            }
            if (hunted.has(thing)) {
                Logger.warning('Someone else is already hunting that...', this, { thing: thing });
                return;
            }
            Logger.info('Target aquired', this, { target: thing });
            this._action = Actions.SEEKING;
            this._seeking = thing;
            hunted.add(thing);
        }
        refresh(bot, world) {
            this.base = bot;
            if (this.action === Actions.SEEKING) {
                let target = Entity.findInWorld(this.seeking, world);
                Logger.debug(target === this._seeking ? 'I refreshed, but my target is still the same...' : 'Refreshed properly :)', this);
                if (target) {
                    hunted.delete(this._seeking);
                    hunted.add(target);
                    this._seeking = target;
                }
                else
                    this.seek(null);
            }
        }
    }
    BotState.State = State;
    function add(bot) {
        if (stateMap[bot.id] !== void 0)
            return;
        states.push(new State(bot));
        stateMap[bot.id] = states.length - 1;
    }
    BotState.add = add;
    function addAll(bots) {
        bots.forEach(bot => add(bot));
    }
    BotState.addAll = addAll;
    function get(bot) {
        if (stateMap[bot.id] === void 0)
            return null;
        return states[stateMap[bot.id]];
    }
    BotState.get = get;
    function getAll() {
        return states;
    }
    BotState.getAll = getAll;
    function refresh(bot, world) {
        Logger.info('Refreshing bot', null, { bot: bot });
        let state = get(bot);
        if (!state) {
            if (world.turn > 0)
                Logger.warning('Bot not found in tracked states!');
            add(bot);
            return;
        }
        state.refresh(bot, world);
    }
    BotState.refresh = refresh;
    function refreshAll(world) {
        Logger.info('Refreshing all bot states...');
        world.bots.forEach(bot => refresh(bot, world));
    }
    BotState.refreshAll = refreshAll;
})(BotState || (BotState = {}));
const BUILD_THRESHOLD = 3;
Logger.setLevel(Logger.Verbosity.DEBUG);
function play(world) {
    BotState.refreshAll(world);
    BotState.getAll().forEach(bot => {
        let adjacentEnemy = Entity.nearest(bot, world.others);
        if (adjacentEnemy && Entity.distance(bot, adjacentEnemy) === 1) {
            bot.attack(adjacentEnemy);
            return;
        }
        if (bot.is(BotState.Actions.IDLE) && bot.wrenches < BUILD_THRESHOLD)
            bot.seek(Entity.nearest(bot, world.wrenches, BotState.getHunted()));
        if (bot.is(BotState.Actions.SEEKING)) {
            let target = bot.seeking;
            if (bot.x === target.x && bot.y === target.y)
                bot.collect();
            else
                bot.moveTo(target);
        }
        else if (bot.wrenches >= BUILD_THRESHOLD)
            bot.build();
    });
}
